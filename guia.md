# Guia de documentação

## Criando/Editando a documentação
1. Clone o repositório da documentação
1. Intale o gitbook-cli com o camando `npm install -g gitbook-cli`
1. No diretório clonado, execute o comando `gitbook serve` para rodar localmente
1. Edite os arquivos que desejar, com seu editor favorito
1. Commit das alterações feitas
1. Quando o commit chegar a branch master, as alterções serão submetidas a documentação em produção.

> Obs: Lembre-se de adicionar a página criada ao SUMMARY.md para que ele apareça no menu de navegação.

{% include "./template.md" %}