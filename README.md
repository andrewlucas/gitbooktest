


<h1 align="center">AI.lab</h1>

<p align="center">
<img src="/images/logo.jpeg">
<br>
Processos, manuais de uso, guias, e mais! 💻

</p>

<div class="row">
<div class="column">
<h2 align="center">Guides & Processes</h2>
<ul>
<li>🚜 Getting Started</li>
<li>🤖 Engineering Guidelines</li>
<li>🔄 Development Lifecycle </li>
<li>🚢 How to Deploy</li>
<li>🛠 Useful Commands</li>
<li>📖 Engineering Interviews</li>
</ul>

<h2 align="center">Testing</h2>
<ul>
<li>📝 How to QA</li>
</ul>
</div>
<div class="column">
<h2 align="center">Codebase</h2>
<ul>
<li>🙋 Engineering Directory </li>
<li>✔️ Code Reviews</li>
<li>⚛️ React</li>
<li>🖥 Backend</li>
</ul>

<h2 align="center">Infrastructure</h2>
<ul>
<li>🕸 AWS</li>
<li>📮 Redis</li>
<li>⚪ CircleCI</li>
</ul>
</div>
</div>

