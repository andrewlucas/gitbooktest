## Dicas e convenções

### Imagem

Para inserir uma imagem centralizada utilize:

```html
<div align="center"><img src="/images/logo.jpeg"></div>
```

<div align="center"><img  src="/images/logo.jpeg"></div>

### Títulos
Uma convenção para títulos

```md
# Título da página
## Título de tópico
### Subtítulo de tópico 1
#### Subtítulo de tópico 2
##### Subtítulo de tópico 3
###### Subtítulo de tópico 4
```

### Formatação de texto
#### Cores
```html
<p style="color: red;">Texto vermelho</p>
```
#### Destaques

```md
_Itálico_ ou *Itálico*
__negrito__ ou **negrito**
```

